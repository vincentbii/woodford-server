<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {

    Route::post('users/login', 'AuthController@login');
    Route::post('users', 'AuthController@register');

    Route::get('user', 'UserController@index');
    Route::match(['put', 'patch'], 'user', 'UserController@update');

    Route::get('profiles/{user}', 'ProfileController@show');
    Route::post('profiles/{user}/follow', 'ProfileController@follow');
    Route::delete('profiles/{user}/follow', 'ProfileController@unFollow');

    Route::get('articles/feed', 'FeedController@index');
    Route::post('articles/{article}/favorite', 'FavoriteController@add');
    Route::delete('articles/{article}/favorite', 'FavoriteController@remove');

    Route::resource('articles', 'ArticleController', [
        'except' => [
            'create', 'edit'
        ]
    ]);

    Route::resource('articles/{article}/comments', 'CommentController', [
        'only' => [
            'index', 'store', 'destroy'
        ]
    ]);

    Route::get('tags', 'TagController@index');
    Route::get('tags2', 'TagController@index2')->name('tags2');

    Route::get('deals', 'Location\AreaController@deals');
    Route::get('formSetup/{id}', 'Location\AreaController@formSetup');
    Route::get('timezones', 'TimezoneController@index');
    Route::get('payment_due_action', 'Location\LocationSessionController@payment_due_action');
    Route::get('currency', 'Location\LocationSessionController@currency');

    Route::resource('articles/{article}/comments', 'CommentController', [
        'only' => [
            'index', 'store', 'destroy'
        ]
    ]);

    Route::get('expenses', 'Location\AreaController@estimated_monthly_expenses');

    Route::get('LocationNotes', 'Location\AreaController@LocationNotes');
    Route::resource('areas', 'Location\AreaController');
    Route::get('cities', 'Location\AreaController@getCities');
    Route::get('noteTypes', 'Location\AreaController@noteType');
    Route::get('countries', 'Location\AreaController@getCountries');
    Route::get('states', 'Location\AreaController@getStates');
    Route::get('onewaydrops', 'Location\AreaController@OneWayDrops');
    Route::get('fleet', 'Location\AreaController@fleet');
    Route::resource('category', 'CategoryController');

    Route::get('lblUnitStatusAfterReturn', 'Location\LocationController@lblUnitStatusAfterReturn');
    Route::get('sessions', 'Location\LocationSessionController@index');
    Route::put('sessions/{id}', 'Location\LocationSessionController@update');
    Route::resource('regions', 'Location\RegionsController');
    Route::get('rate_code', 'Location\LocationController@rateCode');
    Route::get('default_rate_plans', 'Location\LocationController@getRatePlans');
    Route::resource('locations', 'Location\LocationController');
    Route::get('locations2', 'Location\LocationController@index2');
    // Route::post('locations', 'Location\RegionsController@storeLocation');
    // Route::put('locations/{id}', 'Location\LocationController@update');

});