<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estimated_monthly_expenses extends Model
{
    protected $table = 'estimated_monthly_expenses';

    public function location()
    {
        return $this->belongsTo(Location::class, 'location');
    }
}
