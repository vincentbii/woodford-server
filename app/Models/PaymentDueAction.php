<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentDueAction extends Model
{
    protected $table = 'payment_due_action';
}
