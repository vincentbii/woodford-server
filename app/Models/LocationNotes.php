<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocationNotes extends Model
{
	protected $table = 'location_notes';
	
    public function location()
    {
        return $this->belongsTo(Location::class, 'location');
    }
}
