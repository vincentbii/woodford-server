<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fleet_availability_shares extends Model
{
    protected $table = 'fleet_availability_shares';

    public function location()
    {
        return $this->belongsTo(Location::class, 'location');
    }

    public function city()
    {
        return $this->belongsTo(Cities::class, 'city');
    }
    public function state()
    {
        return $this->belongsTo(States::class, 'state');
    }
}
