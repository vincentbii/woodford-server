<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocationSession extends Model
{

    protected $table = 'location_sessions';

     protected $fillable = [
        'user', 'location'
    ];

    public function location()
    {
        return $this->belongsTo(Location::class, 'location');
    }
}
