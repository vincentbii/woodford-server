<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OneWayDrops extends Model
{
    protected $table = 'one_way_drops';

    public function location()
    {
        return $this->belongsTo(Location::class, 'location');
    }

    public function city()
    {
        return $this->belongsTo(Cities::class, 'city');
    }
    public function state()
    {
        return $this->belongsTo(States::class, 'state');
    }
}
