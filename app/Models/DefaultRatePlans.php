<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DefaultRatePlans extends Model
{
    protected $table = 'default_rate_plans';
}
