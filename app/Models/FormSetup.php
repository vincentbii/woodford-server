<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormSetup extends Model
{
    protected $table = 'form_setup';

    public function deals()
    {
        return $this->belongsTo(Deals::class, 'deal');
    }
}
