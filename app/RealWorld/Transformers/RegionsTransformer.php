<?php

namespace App\RealWorld\Transformers;

class RegionsTransformer extends Transformer
{
    protected $resourceName = 'regions';

    public function transform($data)
    {
        return [
        	'id'	=>	$data['id'],
            'code'     => $data['code'],
            'description'     => $data['description'],
            'contact'  => $data['contact'],
            'street1'       => $data['street1'],
            'street2'     => $data['street2'],
            'phone'		=>	$data['phone'],
            'city'		=>	$data['city'],
            'state'		=>	$data['state'],
            'postal_code'	=>	$data['postal_code'],
        ];
    }
}