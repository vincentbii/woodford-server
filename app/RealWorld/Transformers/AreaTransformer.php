<?php

namespace App\RealWorld\Transformers;

class AreaTransformer extends Transformer
{
    protected $resourceName = 'area';

    public function transform($data)
    {
        return [
        	'id'	=>	$data['id'],
            'code'     => $data['code'],
            'description'     => $data['description'],
            'region'        =>  $data['region'],
            'contact'  => $data['contact'],
            'street1'       => $data['street1'],
            'street2'     => $data['street2'],
            'phone'		=>	$data['phone'],
            'city'		=>	$data['city'],
            'state'		=>	$data['state'],
            'postal_address'	=>	$data['postal_address'],
        ];
    }
}