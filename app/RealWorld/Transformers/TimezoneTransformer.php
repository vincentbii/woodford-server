<?php

namespace App\RealWorld\Transformers;

class TimezoneTransformer extends Transformer
{
    protected $resourceName = 'timezones';

    public function transform($data)
    {
        return [
            $data['abbreviation'],
            $data['time_start'],
            $data['gmt_offset'],
            $data['dst'],
        ];
    }
}