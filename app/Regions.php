<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regions extends Model
{
	// protected $primaryKey = "id";

    protected $fillable = [
        'code', 'contact', 'description', 'street1', 'street2', 'postal_code', 'city', 'state', 'phone'
    ];
}
