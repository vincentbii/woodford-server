<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Timezone;
use App\Http\Controllers\Controller;
use App\RealWorld\Transformers\TimezoneTransformer;

class TimezoneController extends ApiController
{
	public function __construct(TimezoneTransformer $transformer)
    {
        $this->transformer = $transformer;

        // $this->middleware('auth.api')->only('index');
    }


    public function index($value='')
    {
    	$timezones = Timezone::with('zone')->get();

        return response()->json($timezones);
        // return $this->respondWithTransformer($timezones);
    }
}
