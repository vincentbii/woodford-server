<?php

namespace App\Http\Controllers\Api\Location;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\ApiController;
use App\Models\Location;
use App\Http\Requests\Api\UpdateLocation;
use App\Models\rateCode;
use App\Models\lblUnitStatusAfterReturn;
use App\Models\DefaultRatePlans;
use App\RealWorld\Paginate\Paginate;
use App\Http\Requests\Api\CreateLocation;

class LocationController extends ApiController
{

    public function __construct()
    {
        // $this->transformer = $transformer;

        $this->middleware('auth.api');
        // $this->middleware('auth.api:optional')->only(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::all();

        return response()->json([
            'locations' => $locations
        ]);
    }

    public function index2()
    {
        $locations = Location::paginate(10);
        return response()->json($locations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateLocation $request)
    {
        $loc_code = $request->input('location.loc_code');
        $location_name = $request->input('location.location_name');
        $area_id = $request->input('location.area_id');

        $location = new Location();
        $location->loc_code = $loc_code;
        $location->location_name = $location_name;
        $location->area_id = $area_id;
        $location->dba_name = $request->input('location.dba_name');
        $location->street1 = $request->input('location.street1');
        $location->street2 = $request->input('location.street2');
        $location->city = $request->input('location.city');
        $location->country = $request->input('location.country');
        $location->state = $request->input('location.state');
        $location->postal_code = $request->input('location.postal_code');
        $location->fax_number = $request->input('location.fax_number');
        $location->phone_number = $request->input('location.phone_number');
        $location->timezone = $request->input('location.timezone');
        $location->franchise_type = $request->input('location.franchise_type');
        $location->last_ra_number = $request->input('location.last_ra_number');
        $location->default_rate_plan = $request->input('location.default_rate_plan');
        $location->rate_code = $request->input('location.rate_code');
        $location->location_email = $request->input('location.location_email');
        $location->send_rez_emails = $request->input('location.send_rez_emails');
        $location->email_on_update = $request->input('location.email_on_update');
        $location->rem_days = $request->input('location.rem_days');
        $location->send_rez_emails_w_form = $request->input('location.send_rez_emails_w_form');
        $location->email_bcc = $request->input('location.email_bcc');
        $location->physical_inventory_group = $request->input('location.physical_inventory_group');
        $location->unit_status_after_return = $request->input('location.unit_status_after_return');
        $location->property_id = $request->input('location.property_id');
        $location->government_id = $request->input('location.government_id');
        $location->minimum_age = $request->input('location.minimum_age');
        $location->grace_period = $request->input('location.grace_period');
        $location->next_day_cutoff = $request->input('location.next_day_cutoff');
        $location->fixed_return = $request->input('location.fixed_return');
        $location->fuel_per_km = $request->input('location.fuel_per_km');
        $location->quote_expires = $request->input('location.quote_expires');
        $location->days_in_rental_month = $request->input('location.days_in_rental_month');
        $location->maintenance_hold = $request->input('location.maintenance_hold');
        $location->max_days_to_backdate_return = $request->input('location.max_days_to_backdate_return');
        $location->shop_id = $request->input('location.shop_id');
        $location->quote_expires = $request->input('location.quote_expires');
        $location->default_fuel_level = $request->input('location.default_fuel_level');
        $location->end_of_gl_period = $request->input('location.end_of_gl_period');
        $location->owning_group_code = $request->input('location.owning_group_code');
        $location->currency = $request->input('location.currency');
        $location->convert_rez_to_local_currency = $request->input('location.convert_rez_to_local_currency');
        $location->payment_due_action_at_open = $request->input('location.payment_due_action_at_open');
        $location->payment_due_action_at_return = $request->input('location.payment_due_action_at_return');
        $location->payment_due_action_at_edit = $request->input('location.payment_due_action_at_edit');
        if($location->save()){
            return response()->json($location, 200);
        }else{
            return response()->json(['Location Not Created!'], 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function getRatePlans(){
        $plans = DefaultRatePlans::all();
        return response()->json($plans);
    }

    public function rateCode($value='')
    {
        $rateCode = rateCode::all();
        return response()->json($rateCode);
    }


    public function lblUnitStatusAfterReturn($value='')
    {
        $lblUnitStatusAfterReturn = lblUnitStatusAfterReturn::all();
        return response()->json($lblUnitStatusAfterReturn);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLocation $request, $id)
    {
        $location = Location::find($id);
        $location->location_name = $request->input('location.location_name');
        $location->loc_code = $request->input('location.loc_code');
        $location->area_id = $request->input('location.area_id');
        $location->dba_name = $request->input('location.dba_name');
        $location->street1 = $request->input('location.street1');
        $location->street2 = $request->input('location.street2');
        $location->city = $request->input('location.city');
        $location->country = $request->input('location.country');
        $location->state = $request->input('location.state');
        $location->postal_code = $request->input('location.postal_code');
        $location->fax_number = $request->input('location.fax_number');
        $location->phone_number = $request->input('location.phone_number');
        $location->timezone = $request->input('location.timezone');
        $location->franchise_type = $request->input('location.franchise_type');
        $location->ra_number = $request->input('location.ra_number');
        $location->default_rate_plan = $request->input('location.default_rate_plan');
        $location->rate_code = $request->input('location.rate_code');
        $location->location_email = $request->input('location.location_email');
        $location->send_rez_email = $request->input('location.send_rez_email');
        $location->email_on_update = $request->input('location.email_on_update');
        $location->rem_days = $request->input('location.rem_days');
        $location->send_rez_email_w_form = $request->input('location.send_rez_email_w_form');
        $location->email_bcc = $request->input('location.email_bcc');
        $location->physical_inventory_group = $request->input('location.physical_inventory_group');
        $location->unit_status_after_return = $request->input('location.unit_status_after_return');
        $location->property_id = $request->input('location.property_id');
        $location->government_id = $request->input('location.government_id');
        if($request->input('location.change_age') == TRUE){
            $location->minimum_age = $request->input('location.minimum_age');    
        }
        $location->purge_years = $request->input('location.purge_years');
        $location->default_fuel_level = $request->input('location.default_fuel_level');
        $location->check_maintenance = $request->input('location.check_maintenance');
        $location->lor_early = $request->input('location.lor_early');
        $location->lor_optional_service = $request->input('location.lor_optional_service');
        $location->show_grace_period = $request->input('location.show_grace_period');
        $location->grace_period = $request->input('location.grace_period');
        $location->next_day_cutoff = $request->input('location.next_day_cutoff');
        $location->shop_id = $request->input('location.shop_id');
        $location->fixed_return = $request->input('location.fixed_return');
        $location->fuel_per_km = $request->input('location.fuel_per_km');
        $location->quote_expires = $request->input('location.quote_expires');
        $location->days_in_rental_month = $request->input('location.days_in_rental_month');
        $location->maintenance_hold = $request->input('location.maintenance_hold');
        $location->max_days_to_backdate_return = $request->input('location.max_days_to_backdate_return');
        $location->split_current_gl_period = $request->input('location.split_current_gl_period');
        $location->require_refund = $request->input('location.require_refund');
        $location->also_apply = $request->input('location.also_apply');
        $location->require_payment = $request->input('location.require_payment');
        $location->insurance_certificate = $request->input('location.insurance_certificate');
        $location->end_of_gl_period = $request->input('location.end_of_gl_period');
        $location->owning_group_code = $request->input('location.owning_group_code');
        $location->currency = $request->input('location.currency');
        $location->convert_rez_to_local_currency = $request->input('location.convert_rez_to_local_currency');
        $location->payment_due_action_at_open = $request->input('location.payment_due_action_at_open');
        $location->payment_due_action_at_return = $request->input('location.payment_due_action_at_return');
        $location->payment_due_action_at_edit = $request->input('location.payment_due_action_at_edit');
        if($location->save()){
            return response()->json($location, 200);
        }else{
            return response()->json("Location Not Updated. Check your filed values", 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Location::destroy($id);
        return response()->json("Deleted Succeessfully!");
    }
}
