<?php

namespace App\Http\Controllers\Api\Location;

use App\Http\Requests\Api\CreateRegion;
use App\Http\Requests\Api\CreateLocation;
use App\Http\Controllers\Controller;
use App\Regions;
use App\Models\Location;
// use App\Http\Controllers\Api\ApiController;
use App\RealWorld\Paginate\Paginate;
use App\RealWorld\Transformers\RegionsTransformer;

class RegionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->transformer = $transformer;

        // $this->transformer = $transformer;
        $this->middleware('auth.api');
    }

    public function index(Regions $regions)
    {
        $regions = Regions::all();

        return response()->json([
            'regions' => $regions
        ]);
        // $regions = $regions->get();
        // return $this->respondWithTransformer($regions);

        // $user = Regions::find(30);
        // return $user;
    }

    public function storeLocation(CreateLocation $request)
    {
        $loc_code = $request->input('location.loc_code');
        $location_name = $request->input('location.location_name');
        $area_id = $request->input('location.area_id');

        $location = new Location();
        $location->loc_code = $loc_code;
        $location->location_name = $location_name;
        $location->area_id = $area_id;
        $location->dba_name = $request->input('location.dba_name');
        $location->street1 = $request->input('location.street1');
        $location->street2 = $request->input('location.street2');
        $location->city = $request->input('location.city');
        $location->country = $request->input('location.country');
        $location->state = $request->input('location.state');
        $location->postal_code = $request->input('location.postal_code');
        $location->fax_number = $request->input('location.fax_number');
        $location->phone_number = $request->input('location.phone_number');
        $location->timezone = $request->input('location.timezone');
        $location->franchise_type = $request->input('location.franchise_type');
        $location->last_ra_number = $request->input('location.last_ra_number');
        $location->default_rate_plan = $request->input('location.default_rate_plan');
        $location->rate_code = $request->input('location.rate_code');
        $location->location_email = $request->input('location.location_email');
        $location->send_rez_emails = $request->input('location.send_rez_emails');
        $location->email_on_update = $request->input('location.email_on_update');
        $location->rem_days = $request->input('location.rem_days');
        $location->send_rez_emails_w_form = $request->input('location.send_rez_emails_w_form');
        $location->email_bcc = $request->input('location.email_bcc');
        $location->physical_inventory_group = $request->input('location.physical_inventory_group');
        $location->unit_status_after_return = $request->input('location.unit_status_after_return');
        $location->property_id = $request->input('location.property_id');
        $location->government_id = $request->input('location.government_id');
        $location->minimum_age = $request->input('location.minimum_age');
        $location->grace_period = $request->input('location.grace_period');
        $location->next_day_cutoff = $request->input('location.next_day_cutoff');
        $location->fixed_return = $request->input('location.fixed_return');
        $location->fuel_per_km = $request->input('location.fuel_per_km');
        $location->quote_expires = $request->input('location.quote_expires');
        $location->days_in_rental_month = $request->input('location.days_in_rental_month');
        $location->maintenance_hold = $request->input('location.maintenance_hold');
        $location->max_days_to_backdate_return = $request->input('location.max_days_to_backdate_return');
        $location->end_of_gl_period = $request->input('location.end_of_gl_period');
        $location->owning_group_code = $request->input('location.owning_group_code');
        $location->currency = $request->input('location.currency');
        $location->convert_rez_to_local_currency = $request->input('location.convert_rez_to_local_currency');
        $location->payment_due_action_at_open = $request->input('location.payment_due_action_at_open');
        $location->payment_due_action_at_return = $request->input('location.payment_due_action_at_return');
        $location->payment_due_action_at_edit = $request->input('location.payment_due_action_at_edit');
        if($location->save()){
            return response()->json($location, 200);
        }else{
            return response()->json(['Location Not Created!'], 401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRegion $request)
    {
        // $insert = \DB::table('test')
        //     ->insert([
        //         'datas' =>  $request->input('region.code')
        //         ]);
        $regions = new Regions();
        $article = $regions->insert([
            'code' => $request->input('region.code'),
            'description' => $request->input('region.description'),
            'contact' => $request->input('region.contact'),
            'street1' => $request->input('region.street1'),
            'street2' => $request->input('region.street2'),
            'city' => $request->input('region.city'),
            'phone' => $request->input('region.phone'),
            'state' => $request->input('region.state'),
            'postal_code'   =>  $request->input('region.postal_code')
        ]);
        // return dd($article);
        return response()->json([
            'region' => $article
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateRegion $request, $id)
    {
        // Regions::where('id', $id)
        //   ->update(['code' => 1]);
        \DB::table('regions')->where('id', $id)->update([
            'code' => $request->input('region.code'),
            'description' => $request->input('region.description'),
            'contact' => $request->input('region.contact'),
            'street1' => $request->input('region.street1'),
            'street2' => $request->input('region.street2'),
            'city' => $request->input('region.city'),
            'phone' => $request->input('region.phone'),
            'state' => $request->input('region.state'),
            'postal_code'   =>  $request->input('region.postal_code')
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $region = Regions::where('id', $id)->delete();
        if($region){
            return $this->respondSuccess();
        }else{
            return response()->json("This item is used by existing areas. Kindly delete them first!");
        }
        
    }
}
