<?php

namespace App\Http\Controllers\Api\Location;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Cities;
use App\Models\Countries;
use App\Models\LocationNotes;
use App\Http\Requests\Api\AreaRequest;
use App\Models\States;
use App\Models\Estimated_monthly_expenses;
use App\Models\Language;
use App\Models\NoteType;
use App\Models\Deals;
use App\Models\FormSetup;
use App\Models\Fleet_availability_shares;
use App\Models\OneWayDrops;
use App\RealWorld\Paginate\Paginate;
use App\Http\Controllers\Api\ApiController;
use App\RealWorld\Transformers\AreaTransformer;


class AreaController extends Controller
{

    public function __construct(AreaTransformer $transformer)
    {
        // $this->transformer = $transformer;

        $this->transformer = $transformer;
        // $this->middleware('auth.api');
    }

    public function deals($value='')
    {
        $deals = Deals::all();

        return response()->json([
            'deals' => $deals
            ]);
    }

    public function estimated_monthly_expenses($value='')
    {
        $expenses = Estimated_monthly_expenses::with('location:id,location_name,loc_code')->get();
        return response()->json($expenses);
    }

    public function formSetup($deal)
    {
        if($deal == 0){
            $setup = FormSetup::all();
        }else{
            $setup = FormSetup::where('deal', $deal)->get();
        }
        

        return response()->json([
            'setup' =>  $setup
            ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $areas = Area::all();
        return response()->json([
            'areas' => $areas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function fleet($value='')
    {
        $availability = Fleet_availability_shares::with('location:id,location_name,loc_code','city:id,name','state:id,name')->get();
        return response()->json($availability);
    }

    public function OneWayDrops($value='')
    {
        $drops = OneWayDrops::with('location:id,location_name,loc_code', 'state:id,name', 'city:id,name')->get();
        return response()->json($drops);
    }

    public function noteType($value='')
    {
        $language = Language::all();
        $types = NoteType::all();
        return response()->json([
            'types' =>  $types,
            'language'  => $language
            ]);
    }

    public function LocationNotes($value='')
    {
        $LocationNotes = LocationNotes::with('location:id,location_name,loc_code')->get();
        return response()->json($LocationNotes);
    }

    public function getCities($value='')
    {
        $cities = Cities::all();
        return response()->json([
            'cities' => $cities
        ]);
    }

    public function getStates($value='')
    {
        $states = States::all();
        return response()->json([
            'states'    =>  $states
            ]);
    }

    public function getCountries($value='')
    {
        $cities = Cities::all();
        $states = States::all();
        $countries = Countries::all();
        return response()->json([
            'countries'    =>  $countries
            ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AreaRequest $request)
    {
        $areas = new Area();
        $article = $areas->insert([
            'code' => $request->input('area.code'),
            'description' => $request->input('area.description'),
            'region'    =>  $request->input('area.region'),
            'contact' => $request->input('area.contact'),
            'street1' => $request->input('area.street1'),
            'street2' => $request->input('area.street2'),
            'city' => $request->input('area.city'),
            'phone' => $request->input('area.phone'),
            'state' => $request->input('area.state'),
            'postal_code'   =>  $request->input('area.postal_code')
        ]);
        // return dd($article);
        return response()->json([
            'area' => $article
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AreaRequest $request, $id)
    {
        $areas = Area::where('id', $id)->update([
            'code' => $request->input('area.code'),
            'description' => $request->input('area.description'),
            'region'    =>  $request->input('area.region'),
            'contact' => $request->input('area.contact'),
            'street1' => $request->input('area.street1'),
            'street2' => $request->input('area.street2'),
            'city' => $request->input('area.city'),
            'phone' => $request->input('area.phone'),
            'state' => $request->input('area.state'),
            'postal_code'   =>  $request->input('area.postal_code')
            ]);

        return response()->json([
            'area' => $areas
        ]);

        return $request->input('product.product_name');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $area = Area::where('id', $id)->delete();

        return response()->json($id);
    }
}
