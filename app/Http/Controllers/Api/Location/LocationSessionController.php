<?php

namespace App\Http\Controllers\Api\Location;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LocationSession;
use App\Http\Controllers\Api\ApiController;
use App\RealWorld\Paginate\Paginate;
use App\Models\PaymentDueAction;
use App\Models\Currency;

class LocationSessionController extends ApiController
{

    public function __construct()
    {
        // $this->transformer = $transformer;

        $this->middleware('auth.api');
        // $this->middleware('auth.api:optional')->only(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sessions = LocationSession::with('location')
            ->where('status', 1)
            ->select('id', 'user', 'location')
            ->get();

        return response()->json(['sessions' => $sessions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = \DB::table('location_sessions')
            ->insert([
                'user'  =>  $request->input('session.user'),
                'location'  =>  $request->input('session.location_id'),
                'status'    => 1
                ]);
        // return response()->json([
        //     'sessions' => $data
        // ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $delete = LocationSession::where('user', $request->input('user'))->update(array('status' => 0));
        $session = new LocationSession();
        $session->user = $request->input('user');
        $session->status = 1;
        $session->location = $id;
        $session->save();
        // return response()->json($request);
        return $this->respondSuccess();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function payment_due_action()
    {
        $payment_due_action = PaymentDueAction::all();
        return response()->json($payment_due_action);
    }

    public function currency($value='')
    {
        $currencies = Currency::all();
        return response()->json($currencies);
    }
}
