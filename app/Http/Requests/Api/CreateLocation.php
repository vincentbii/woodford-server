<?php

namespace App\Http\Requests\Api;

class CreateLocation extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function validationData()
    {
        return $this->get('location') ?: [];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'loc_code' => 'required|string|max:4|unique',
            'location_name' => 'required|string|max:255|unique:locations,location_name',
            'area_id'     =>  'required'
        ];
    }
}
