<?php

namespace App\Http\Requests\Api;

class CreateRegion extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function validationData()
    {
        return $this->get('region') ?: [];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|string|max:4',
            'description' => 'required|string|max:255'
        ];
    }
}
