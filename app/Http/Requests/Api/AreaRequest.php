<?php

namespace App\Http\Requests\Api;

class AreaRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function validationData()
    {
        return $this->get('area') ?: [];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|string|max:255|unique:area,code',
            'description' => 'required|string|max:255|unique:area,description',
            'region' => 'required',
        ];
    }
}
