<?php

namespace App\Http\Requests\Api;


class UpdateLocation extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function validationData()
    {
        return $this->get('location') ?: [];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location_name' => 'required|string|max:255',
            'area_id'     =>  'required'
        ];
    }
}
